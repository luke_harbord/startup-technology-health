# Startup Technology Health

## Introduction

This is list is supposed to help technology leaders of early stage, product-focused businesses assess the overall technology health of their startup across a number of categories. 

## What this list *is not*

This guide is *not* designed to be checked off completely, but serve a guide to technolgoy leaders to self-asess and unearth the parts of their technology stack that need prioritisation. It is rare that every single one of these items be checked off - different areas will have different levels of focus as a product makes it out of the idea phase and into production and maturity. 

## Contents 

The list is divided up into several high-level areas of focus for technology leaders:

1) [Platform engineering](#platform-engineering)
2) [Product development](#product-development)
3) [Operational technology](#operational-technology)
4) [Stakeholder management](#stakeholder-management)
5) [Security](#security)
6) [Data protection](#data-protection)
7) [Culture](#team-and-culture)

## 1) Platform engineering  <a name="platform-engineering"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 1.1   | Can deploy multiple times per day to production   |   |
| 1.2   | Can validate code stability before it is merged   |   |
| 1.3   | Monitors production resources |   |
| 1.4   | Has alerting in place in case of errors |   |
| 1.5   | Has a high leve of release confidence |   |


## 2) Product development <a name="product-development"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 2.1   | Has a clear, demonstrable short-term roadmap   |   |
| 2.2   | Has a clear, demonstrable architecture |   |
| 2.3   | Has a prioritised, estimated backlog |   |
| 2.4   | Understands how users are using the product |   |
| 2.5   | Frequently assesses product/market fit |   |
| 2.6   | Has a clear understanding of its technical debt |   |
| 2.7   | Has a structured approach of working |   |

## 3) Operational technology <a name="operational-technology"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 3.1   | Uses a CRM to manage customer interaction |   |
| 3.2   | Empowers sales, marketing and product teams to solve challenges themselves     |   |

## 4) Stakeholder management <a name="stakeholder-management"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 4.1   | Frequently updates investors, board and senior management team as to progress |   |
| 4.2   | Engages stakeholders in the development process |   |
| 4.3   | Has a structured approach for building stakeholder requirements into backlog |   |

## 5) Security <a name="security"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 5.1   | Has mapped out and understands its attack vectors |   |
| 5.2   | Engages in regular penetration testing |   |
| 5.3   | Tests code for vulnerabilities before it's deployed |   |

## 6) Data protection <a name="data-protection"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 6.1   | Understands and adheres to relevant data protection laws |   |
| 6.2   | Is regularly audited for data protection processes |   |

## 7) Team and Culture <a name="team-and-culture"></a>

My startup...

| Number | Statement | RAG status |
| -- | -- | -- |
| 7.1   | Has a clear career pathway for developers |   |
| 7.2   | Has black and white expectations for every development role |   |
| 7.3   | Has a repeatable, scalable recruitment process |   |
| 7.4   | Has employer branding |   |
| 7.5   | Maintains a sustainable workload and development pace |   |
